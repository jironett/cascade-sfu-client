
class ServerConfig
{
    constructor(servers, selectedServerId)
    {
        if (!Array.isArray(servers) || servers.length === 0)
            throw TypeError("Config for serverConfig class has to be filled as array");

        this.configServers = servers;
        this._selectedServerId = selectedServerId;

        if (!selectedServerId || typeof selectedServerId === "undefined")
            this._selectedServerId = this.firstId();
    }

    selectedServerId(id)
    {
        if (typeof id !== "undefined")
            this._selectedServerId = id;

        return this._selectedServerId;
    }

    firstId()
    {
        return this.configServers[0].id;
    }

    getSocketUrl(conferenceId, roomId, peerName, forceH264)
    {
        const conf = this.config();
        let url = 'wss://' + conf.hostname + ':' + conf.clientPort;
        url += '/?serverId=' + this._selectedServerId + '&peerName=' + peerName;
        url += '&roomId=' + roomId + '&conferenceId=' + conferenceId;

        if (forceH264)
            url += '&forceH264=true';

        return url;
    }

    config()
    {
        for (const server of this.configServers) {
            if (server.id == this._selectedServerId)
                return server;
        }
        return null;
    }
}

module.exports = ServerConfig;